Standard Operating Procedures (SOPs)
====================================

Our laboratory operates using the following SOPs: 

.. toctree::
   :maxdepth: 2

   dna_extraction
   library_prep


