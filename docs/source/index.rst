Welcome to microdisat's documentation!
======================================


This documentation contains all the instruction for running the costmized pipeliness for analyising the structure of microbial communities based on 2x250 bp Illumina seqeuncing of 16S rRNA and ITS. 
Both otu and asv based pipipiles are developed.

First clone the microdisat repository::

        git clone https://gitlab.com/andrea.franzetti/icme9.git




Contents:

.. toctree::
   :maxdepth: 2
   
   sops
   otupipeline
   asvpipeline
   scripts
   
