#!/usr/bin/python

"""
Usage:
count.illumina.py [arg1] [arg2]
[arg1]: file mapping prodotto dallo script Illumina
[arg2]: file rdp prodotto dalle script Illumina
[arg3]: numero per normalizzare
....
"""
    
import io
import sys
import string
import pandas as pd
import numpy as np


if len(sys.argv) == 1:
	print __doc__
	exit(0)


data=pd.read_table(sys.argv[1])
count=data.apply(pd.value_counts).fillna(0)
table=count
table[table.columns[0:]].astype(float)
#if int(sys.argv[3])!=0:
#	table[table.columns[0:]]=int(sys.argv[3])*table[table.columns[0:]]/table[table.columns[0:]].sum()
table.to_csv(open('table.'+sys.argv[1],'w'),sep='\t')
table=pd.read_table('table.'+sys.argv[1])
table.rename(columns={'Unnamed: 0':'otu'}, inplace=True)


names=['otu','blank', 'Domain', 'Rank', 'Domain_c', 'Phylum', 'Rank', 'Phylum_c', 'Class', 'Rank', 'Class_c', 'Order', 'Rank', 'Order_c', 'Family', 'Rank', 'Family_c', 'Genus', 'Rank', 'Genus_c']

rdp=pd.read_table(sys.argv[2],header=None,names=names)


merged=pd.merge(table,rdp,how='left',on='otu')


merged.to_csv(open('merged.'+sys.argv[1]+'.rdp.txt','w'),sep='\t')






		


