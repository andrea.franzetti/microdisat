 #!/bin/bash

Rscript ./dada.script/dada2_filtering_ITS_16S.R 

cp R1-16S/16s-filtered1/* R1/filtered1
cp R2-16S/16s-filtered1/* R2/filtered1
cp R1-ITS/its-filtered1/* R1/filtered1
cp R2-ITS/its-filtered1/* R2/filtered1

Rscript ./dada.script/dada2_asv_inference_ITS_16S.R


