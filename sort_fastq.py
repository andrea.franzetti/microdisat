#!/usr/bin/python

"""
Usage: python sort_fastq.py [fastq1] [fastq2] [barcode file]
Barcode file is a comma-delimited file with barcode information
Barcode file example: 
AGTT,ATTC,Sample1,
GTAA,ATCC,Sample2,
....

"""
    
import io
import sys
import string
import numpy
from Bio import SeqIO
from Bio import Seq
import itertools as it


if len(sys.argv) == 1:
	print __doc__
	exit(0)

      
paired1 = open(sys.argv[1], "r")
paired2 = open(sys.argv[2], "r")
file1=SeqIO.parse(paired1, "fastq")
file2=SeqIO.parse(paired2, "fastq")


bc=open(sys.argv[3], "r")
bclist=bc.readlines()


for z,w in it.izip(file1,file2):
	for line in bclist:
		if z.seq.startswith(line.split(',')[0]) and w.seq.startswith(line.split(',')[1]):
			f=open(line.split(',')[2]+'.R1.fastq','a')
			r=open(line.split(',')[2]+'.R2.fastq','a')
			trimmed_z = z[len(line.split(',')[0]):]
			trimmed_w = w[len(line.split(',')[1]):]
			SeqIO.write(trimmed_z, f, "fastq")
			SeqIO.write(trimmed_w, r, "fastq")

paired1.close()
paired2.close()
