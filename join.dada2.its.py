#!/usr/bin/python

"""
This script 

Usage:

join.py [list of dada2 tables] [output file]

"""
import io
import sys
import string
import pandas as pd
import numpy as np

if len(sys.argv) == 1:
	print (__doc__)
	exit(0)

print("*Loading and merging each ASV abundance file \n")


cov1=pd.read_csv(sys.argv[1],sep='\t')
print(sys.argv[1].split('/')[-1])
taxa1=cov1[['ASV_seq','Kingdom','Phylum','Class', 'Order','Family','Genus','Species']]
#cov1=cov1.drop(['ASV','Kingdom','Phylum','Class', 'Order','Family','Genus','Species'], axis=1)
cov1=cov1.drop(['Kingdom','Phylum','Class', 'Order','Family','Genus','Species'], axis=1)

for sample in sys.argv[2:-1]:        
        #print("Considering",sample)
        print(sample.split('/')[-1])
        head=(sample.split('/')[-1])
        cov=pd.read_csv(sample,sep='\t')
        taxa=cov[['ASV_seq','Kingdom','Phylum','Class', 'Order','Family','Genus','Species']]
        #print type(taxa1)
        #print type(taxa)
        taxa1.append(taxa)
        #cov=cov.drop(['ASV','Kingdom','Phylum','Class', 'Order','Family','Genus','Species'], axis=1)
        cov=cov.drop(['Kingdom','Phylum','Class', 'Order','Family','Genus','Species'], axis=1)
        cov1=pd.merge(cov1,cov,on=['ASV_seq'],how='outer')
taxa2=taxa1.drop_duplicates()
cov1.fillna(0, inplace=True)
cov1.drop_duplicates(subset='ASV_seq',inplace=True)
#print tax_ts
merge=pd.merge(cov1,taxa2,on=['ASV_seq'],how='left')
#merge.fillna(0, inplace=True)
merge.insert(1, 'ASV', merge.index)
print merge
merge.to_csv(sys.argv[-1],sep="\t",index=False)

#=============================================================
