#!/bin/bash


#Script per pipeline "Analisi ampliconi 16S da Illumina" ver 11/05/2016.
#Analisi basate su usearch v8 

read -p "Inserisci il nome del progetto " pr

read -p "Inserisci la lista dei campioni " list


echo "Creo un file log..."
echo "File Log Illumina" > file.log.txt
echo `date` >> file.log.txt


for i in $list; 

do

echo "Riduciamo la lunghezza delle reads e eseguiamo merging, quality filtering...."

python /home/andrea/scripts/split_fastq.py ${i}.R1.fastq 160
python /home/andrea/scripts/split_fastq.py ${i}.R2.fastq 160



#sickle pe -q 30 -l 150 -x -f cat.$i.R1.fastq -r cat.$i.R2.fastq -t sanger -o sk.$i.R1.fastq -p sk.$i.R2.fastq -s sk.single.$i.fastq
usearch8 -fastq_mergepairs cut.${i}.R1.fastq -reverse cut.${i}.R2.fastq -fastaout filtered.merged.$i.fasta -fastq_maxdiffs 0  -fastq_merge_maxee 0.5

#echo "Numero sequenze file $i merged" >> file.log.txt
#grep ">" filtered.merged.$i.fasta | wc -l >> file.log.txt

done


echo "Concatena i file di sequenze..."
cat filtered.merged.*.fasta > $pr.cat.fasta



echo "Eseguiamo dereplication, size sorting e clustering..."
#vsearch -derep_fulllength $pr.cat.fasta -output uniques.ns.$pr.cat.fasta -sizeout -minuniquesize 2
usearch8 -derep_fulllength $pr.cat.fasta -fastaout uniques.ns.$pr.cat.fasta -sizeout -minuniquesize 2
usearch8 -cluster_otus uniques.ns.$pr.cat.fasta -otus otus.$pr.fasta -uparseout out.up -relabel OTU_ -sizein -sizeout


sed 's/;.*;//g' otus.$pr.fasta > renamed.otus.$pr.fasta 
#python /usr/local/bin/python_scripts/fasta_number.py otus.$pr.fasta OTU_ > renamed.otus.$pr.fasta

echo "Contiamo il numero di sequenze chimeriche..."
echo "Numero sequenze chimeriche" >> file.log.txt
grep "chimera" out.up | wc -l >> file.log.txt

echo "Mappiamo le sequenze di ciascun file verso le sequenze rappresentative delle OTU..."

for i in $list;

do
usearch8 -usearch_global filtered.merged.$i.fasta -db renamed.otus.$pr.fasta -id 0.97 -strand plus -userout mapping.$i.tg -userfields target -uc mapping.97.$i.uc

done 

echo "Contiamo il numero di sequenze mappanti..."
wc -l mapping*tg

read -p "Inserisci il valore a cui normalizzare le abbondanze (0 per non normalizzare): " num

echo "Normalizziamo e formattiamo i files..."

for i in $list;

do

cut -f1 -d ';' mapping.$i.tg > cut.mapping.$i.tg
sed "1i $i" cut.mapping.$i.tg > hd.cut.sub.mapping.$i.tg

done

paste hd.cut.sub.mapping* > hd.$pr.mapping.txt

echo "Classifichiamo le sequenze rappresentative delle OTU con RDP...."

java -Xmx1g -jar /usr/local/bioinf/rdp_classifier_2.11/dist/classifier.jar classify -c 0.8 -o $pr.otu.rdp.txt -h $pr.otu.rdp.hier.txt -f fixrank  renamed.otus.$pr.fasta

echo "Eseguiamo il merging delle tabelle ottenute..."

python /usr/local/scripts/count_illumina.py hd.$pr.mapping.txt $pr.otu.rdp.txt $num








